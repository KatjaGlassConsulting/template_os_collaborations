# Setup Repository (Create a copy, update and add members)

To use the template to create your own workshop repository, you can create a new repository using the template as source.

You could also create a "fork", but you can only have one fork, meaning if you plan multiple collabortions, you can use the "fork" way just once. For this the instructions below are to create a new repository based on another.

## Create new repository from template

To create a new repository based from this template, please perform the following steps:

- click the "+" in the top navigation -> "New project/repository"
- Select "import project"
- Select "Repo by URL"
- Enter the URL containing the .git postfix:
  - `https://gitlab.com/KatjaGlassConsulting/template_os_collaborations.git`
- Edit project name and description
- Make the repository public to allow others to see and contribute
- Click "Create project"

The following annimation shows the steps:

![Animation to show how to create repository from template](./img/how_to_setup_create_repo.gif)

## Update repository

As a next steps you need to update a few things which are noted in the README.md. The easiest way to update the repository is through the integrated Development environment within GitLab which can easily be started throuth the "Web IDE" button.

The following steps might be performed:

- click the "Web IDE" to start a nice working environment
- click the files to updates 
  - README.md needs to be updated and contains also a list of what should be updated
  - Next to the "Edit" tab at the top, there is also a "Preview" tab where the final formatting can be seen
  - Optionally you might want to remove some "How To's"
- After all changes are done, these needs to be "committed" 
  - commit into the "main" branch to have it direcly be visible

The following animation shows the editing:

![Animation to show how to edit the repository](./img/how_to_setup_edit_repo.gif)

## Grant members access

Project and Workshop members should be able to work with the repository as well. For this you have to add the people to the repository members, ideally as Developer.

The following steps can be performed:

- Go to "Project information" -> "Members"
  - This is contained in the left navigation, the top point
  - You can toggle the left navigation with the left-bottom arrows
- Search for the GitLab names under Members and add all in the members field
- select "Developer" as role
- Press the "Invite" button
- At the bottom you can see all members including yourself

The following animation shows the member adding:

![Guide on how to add members to a repository](./img/how_to_add_members.gif)

Now the Workshop repository is setup to be used by you and all your members visible to the whole world.

